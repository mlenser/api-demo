import React from 'react';
import {
  BrowserRouter,
  Route,
  Switch,
} from 'react-router-dom';
import injectSheet from 'react-jss';
import styles from './App.styles';
import Home from './components/Home/Home';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route component={Home} />
    </Switch>
  </BrowserRouter>
);

export default injectSheet(styles)(App);
