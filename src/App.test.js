import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'react-jss';
import App from './App';

it('renders without crashing', () => { // eslint-disable-line no-undef
  const div = document.createElement('div');
  ReactDOM.render(
    <ThemeProvider theme={{}}>
      <App />
    </ThemeProvider>,
    div,
  );
  ReactDOM.unmountComponentAtNode(div);
});

// TODO: Use enzyme with some runner like Ava, though this won't be done as part of this demo
