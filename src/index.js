import React from 'react';
import ReactDOM from 'react-dom';
import { ThemeProvider } from 'react-jss';
import App from './App';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <ThemeProvider theme={{}}>
    <App />
  </ThemeProvider>,
  document.getElementById('root'),
);
registerServiceWorker();
