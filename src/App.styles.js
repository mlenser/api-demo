export default () => ({
  '@global': {
    body: {
      fontFamily: 'sans-serif',
      margin: 0,
      padding: 0,
    },
  },
});
