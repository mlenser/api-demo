export default () => ({
  app: {
    textAlign: 'center',
  },
  logo: {
    animation: 'logo-spin infinite 20s linear',
    height: 80,
  },
  header: {
    backgroundColor: '#222',
    color: '#FFF',
    height: 150,
    padding: 20,
  },
  title: {
    fontSize: '1.5rem',
  },
  intro: {
    fontSize: 'large',
  },
  '@keyframes logo-spin': {
    '0%': {
      transform: 'rotate(0deg)',
    },
    '100%': {
      transform: 'rotate(360deg)',
    },
  },
});
