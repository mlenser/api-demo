module.exports = {
  extends: [
    'airbnb',
    'plugin:react/recommended'
  ],
  env: {
    browser: true,
    node: true
  },
  plugins: [
    'react'
  ],
  rules: {
    'react/jsx-filename-extension': [
      1,
      {
        extensions: [
          '.js',
          '.jsx',
        ],
      },
    ],
  },
};